// Follow this setup guide to integrate the Deno language server with your editor:
// https://deno.land/manual/getting_started/setup_your_environment
// This enables autocomplete, go to definition, etc.

import { readCSV } from "https://deno.land/x/csv/mod.ts";

const text = Deno.readTextFile(
  "./pokemon.csv"
);

console.log(text);
// for await (const row of readCSV(text)) {
//   console.log('row:')
//   for await (const cell of row) {
//     console.log(`  cell: ${cell}`);
//   }
// }

// f.close();

// To invoke:
// curl -i --location --request POST 'http://localhost:54321/functions/v1/' \
//   --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZS1kZW1vIiwicm9sZSI6ImFub24ifQ.625_WdcF3KHqz5amU0x2X5WWHP-OEs_4qj0ssLNHzTs' \
//   --header 'Content-Type: application/json' \
//   --data '{"name":"Functions"}'
