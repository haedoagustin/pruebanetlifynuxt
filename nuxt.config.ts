import { defineNuxtConfig } from "nuxt";

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  typescript: {
    shim: false,
  },
  modules: ["@nuxtjs/supabase"],
  vue: {
    compilerOptions: {
      isCustomElement: tag => ['icon'].includes(tag)
    }
  }
});
